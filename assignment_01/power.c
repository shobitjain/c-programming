#include <stdio.h>

int int_power(int n, int p) {
        if (p > 1)
        {
            return n * int_power(n, p-1);
        }
        else 
        {
         return n;
        }
    
}

double double_power(double n, int p) {
        if (p > 1)
        {
            return n * double_power(n, p-1);
        }
        else 
        {
         return n;
        }
}

int main() {
    printf("3 ^ 1 = %d\n", int_power(3, 1));
    printf("3 ^ 2 = %d\n", int_power(3, 2));
    printf("3 ^ 3 = %d\n", int_power(3, 3));
    printf("3.0 ^ 3 = %f\n", double_power(3.0, 3));
    printf("3.0 ^ 5 = %f\n", double_power(3.0, 5));
    return 0;
}
