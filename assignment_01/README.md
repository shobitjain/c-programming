# Assigment 01

This is just a simple one to familiarize with the way functions work.

## The goal

Write two functions: `int_power(n, p)` and `double_power(n, p)` that calculate _n_ to the power of _p_ with two different types.

`int_power` should be using an integer argument and `double_power` a double argument and return type.

The functions should both be recursive (that means that they should call themselves).

It's not allowed to use the C standard library function `pow` or `exp(ln())`.

## Template
`power.c`:
```c
#include <stdio.h>

int int_power(int n, int p) {
    // TODO: your code here
}

double double_power(double n, int p) {
    // TODO: your code here
}

int main() {
    printf("3 ^ 3 = %d\n", int_power(3, 3));
    printf("3.0 ^ 3.0 = %f\n", double_power(3.0, 3.0));
    return 0;
}
```

## Submission

You have to create a Gitlab project here on https://gitlab.cs.ttu.ee/

The Project slug should be `its8020-2020` and you should add me as a member (developer role).

Create a folder `assignment_01` and put your code in the file `power.c`
