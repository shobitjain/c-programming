# Assignment 02: Birthday Calculator

**updated**: Added more samples and pushed deadline.

The objective of this assigment is to code a birthay calculator `bdcal` that takes in its input lines with birthdays on it and outputting the next date of the birthday as well as the number of days to it.

## Goal

The goal of this assignment is for you to learn how to parse text input from `stdin` and use the C standard date/time conversion functions.

The input is coming in lines, with each line containing either `MONTH DAY` or `DAY MONTH`.

The month number can be three letters to full names, in other words, you can just match the first three letters.

There will be multiple lines in the input, your program will output for each line the corresponding output in the format "`YYYY-mm-dd` in `d` days".

For any line that you cannot parse, you can skip outputting anything.

Useful functions:
[`fgets`](https://en.cppreference.com/w/c/io/fgets),
[`sscanf`](https://en.cppreference.com/w/c/io/fscanf),
[`mktime`](https://en.cppreference.com/w/c/chrono/mktime),
[`time`](https://en.cppreference.com/w/c/chrono/time),
[`strftime`](https://en.cppreference.com/w/c/chrono/strftime),
[`strncmp`](https://en.cppreference.com/w/c/string/byte/strncmp),
[`tolower`](https://en.cppreference.com/w/c/string/byte/tolower)

## Sample input/output

The input is like this:

```
may 5
dec 25
feb 24
september 1
1 april
OCT 10
June 1st
15 jul
oct 15
30 august
asdf foo
6 dec
This is a really long line that shouldn't be read into any normal string buffers in your code.
29 febr
```

if you run it through the `bdcal` program you should get the output like this:
```
$ ./bdcal < input.txt
2021-05-05 in 202 days
2020-12-25 in 71 days
2021-02-24 in 132 days
2021-09-01 in 321 days
2021-04-01 in 168 days
2021-10-10 in 360 days
2021-06-01 in 229 days
2021-07-15 in 273 days
2021-10-15 in 365 days
2021-08-30 in 319 days
2020-12-06 in 52 days
2021-03-01 in 137 days
```

## Submission

You have to create a Gitlab project here on https://gitlab.cs.ttu.ee/

The Project slug should be `its8020-2020` and you should add me as a member (developer role).

Create a folder `assignment_02` and put your code in the file `bdcal.c`

## Scoring

You will get a total of 3 points for this one.

There will be points deducted for:
- Wrong output.
- Not specifying string lengths in `scanf` `printf` etc.
- Bad formatting, committed binaries
