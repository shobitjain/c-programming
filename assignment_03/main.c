#include <stdio.h>

#include "options.h"
#include "dumper.h"

int main(int argc, char **argv) {

    struct options options;
    if (!options_get(&options, argc, argv)) {
        return 1;
    }

    if (!dump(&options)) {
        fprintf(stderr, "Dumping failed!\n");
        return 2;
    }

    return 0;
}

