#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#include "options.h"

static struct option long_options[] =
    {
        {"help", no_argument, 0, 'h'},
        {"columns", required_argument, 0, 'c'},
        {"output", required_argument, 0, 'o'},
        {0, 0, 0, 0}};

int options_get(struct options *options, int argc, char **argv)
{

    options->inputfile = NULL;
    options->outputfile = NULL;
    options->columns = 16;
    char col_check = 0, out_check = 0, input_check = 0;

    for (int i = 1; i < argc; i++)
    {
        if ((strcmp(argv[i], "--") == 0) && input_check == 0)
        {

            options->inputfile = argv[i + 1];
            input_check = 1;
        }

        if (!input_check && (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0))
        {
            fprintf(stderr,
                    "Usage: %s [-c cols] [-o output] [--] [input]\n"
                    "Options:\n"
                    "  -c, --columns    Set output max columns (min 1, default 16)\n"
                    "  -o, --output     Set output file (stdout default)\n"
                    "  -h, --help       Show this help\n"
                    "  --               Stop interpreting options (filename is \"--help\" etc.)\n"
                    "  input            Read the file (stdin default)\n",
                    argv[0]);
            exit(0);
        }
    }

    int option_index = 0;
    int opt = 0;

    while ((opt = getopt_long(argc, argv, "c:o:",
                              long_options, &option_index)) != -1)
    {
        input_check = 1;
        switch (opt)
        {
        case 'c':
            if (col_check == 0)
            {
                col_check = 1;
                options->columns = atoi(optarg);
            }
            else
            {
                fprintf(stderr, "Only one column should be given\n");
                exit(1);
            }
            if (options->columns <= 0)
            {
                fprintf(stderr, "Col should not be less than 1!\n");
                exit(1);
            }

            break;
        case 'o':
            if (out_check == 0)
            {
                out_check = 1;
                options->outputfile = optarg;
            }
            else
            {
                fprintf(stderr, "Only one output should be given\n");
                exit(1);
            }

            break;
        default:
            exit(EXIT_FAILURE);
        }
    }
    if (argc > optind)
    {
        options->inputfile = argv[optind];
    }
    if (!input_check && argc > 1)
    {
        fprintf(stderr, "Multiple inputs without option!!\n");
        exit(1);
    }

    return 1;
}