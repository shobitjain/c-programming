#include <stdio.h>

int main() {

    int nums[] = {
        1, 2, 3, 4, 5
    };
    size_t len = sizeof(nums)/sizeof(nums[0]);

    for (size_t i = len; i--; i >= 0 ) {
        printf("%d\n", nums[i]);
    }

    return 0;

}