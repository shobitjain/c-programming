#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include "options.h"
#define MIN 0x20
#define MAX 0x7e
int dump(struct options *options)
{
    // (void)options;
    unsigned int cols = 16;

    FILE *fp, *file_out;

    if (options->inputfile != NULL)
    {
        fp = fopen(options->inputfile, "rb");
        if (fp == NULL)
        {
            fprintf(stderr, "File Open Error\n");
            exit(1);
        }
    }
    else
    {
        fp = stdin;
        // printf("stdin selected\n");
    }

    if (options->outputfile != NULL)
    {
        file_out = fopen(options->outputfile, "w");
        if (file_out == NULL)
        {
            fprintf(stderr, "Write File Open Error\n");
            exit(1);
        }
    }
    else
    {
        file_out = stdout;
        //printf("stdout selected\n");
    }

    cols = options->columns;
    int c, nextChar;
    int index = 0, totalCharCount = 0, isLastSegment = 0;
    unsigned int hexAndAsc[cols];
    unsigned int address = 0, char_count = 0;

    rewind(fp);

    while ((c = fgetc(fp)) != EOF)
    {
        char_count++;
        hexAndAsc[index] = c;
        index++;
        totalCharCount++;

        nextChar = fgetc(fp);

        if (nextChar == EOF)
        {
            isLastSegment = 1;
            //printf("Got EOF\n");
            int char_count_diff = cols - char_count;
            if (char_count_diff > 0)
            {
                for (int k = 0; k < char_count_diff; k++)
                {
                    hexAndAsc[index] = '-';
                    char_count++;
                    index++;
                }
            }
        }
        else
        {

            if (fp == stdin)
            {
                hexAndAsc[index] = nextChar;
                index++;
                totalCharCount++;
                char_count++;
            }
            else
            {
                fseek(fp, totalCharCount, SEEK_SET);
            }
        }
        if ((char_count % cols) == 0)
        {

            fprintf(file_out, "%08x | ", address);

            for (size_t i = 0; i < cols; i++)
            {
                if (hexAndAsc[i] == '-' && isLastSegment == 1)
                {

                    fprintf(file_out, "-- ");
                }
                else
                {
                    fprintf(file_out, "%02x ", hexAndAsc[i]);
                }
            }
            fprintf(file_out, "| ");
            for (size_t i = 0; i < cols; i++)
            {

                if (hexAndAsc[i] == '\n')
                {
                    fprintf(file_out, ".");
                }
                else
                {
                    if (hexAndAsc[i] >= MIN && hexAndAsc[i] <= MAX)
                    {
                        fprintf(file_out, "%c", hexAndAsc[i]);
                    }
                    else
                    {
                        fprintf(file_out, ".");
                    }
                }
            }

            index = 0;
            fprintf(file_out, " |\n");
            address += cols;
            char_count = 0;
        }
    }

    fclose(fp);
    fclose(file_out);

    return 1;
}