#include <stdio.h>
#include <time.h>
#include <locale.h>

#define BUF_LEN 256

int main()
{
    // char buff[70];
    // struct tm my_time = { .tm_year=120, // = year 2012
    //                       .tm_mon=9,    // = 10th month
    //                       .tm_mday=9,   // = 9th day
    //                       .tm_hour=8,   // = 8 hours
    //                       .tm_min=10,   // = 10 minutes
    //                       .tm_sec=20    // = 20 secs
    // };
 
    // if (strftime(buff, sizeof buff, "%A %c", &my_time)) {
    //     puts(buff);
    // } else {
    //     puts("strftime failed");
    // }
 
    // setlocale(LC_TIME, "en_US.utf8");
 
    // if (strftime(buff, sizeof buff, "%A %c", &my_time)) {
    //     puts(buff);
    // } else {
    //     puts("strftime failed");
    // }
//    time_t rawtime;
//    struct tm *info;
//    time( &rawtime );
//    info = localtime( &rawtime );
//    printf("Current local time and date: %s", asctime(info));
//    return(0);
    char buf[BUF_LEN] = {0};
    time_t seconds = time(NULL);
    struct tm *ctm = localtime(&seconds);
    
    strftime(buf, BUF_LEN, "%Y-%m-%d", ctm);
    
    puts(buf);
    
    return(0);
}