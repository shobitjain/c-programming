#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

int main()
{
    int k = 0; 
    char monname[3];
   char *months[12]={"jan","feb","mar", "apr","may","jun","jul","aug","sep","oct","nov","dec"};
   scanf("%s", &monname);
   for(int i = 0; monname[i]; i++){
       monname[i] = tolower(monname[i]);
       }
   
   while (k < 12)
   {

    if (strcmp (months[k], monname)==0) {
        printf("%s \t %d \n", monname, k+1);
        break;
    }
    k++; 
   }
   return 0;
}
