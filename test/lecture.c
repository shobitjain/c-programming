#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define BUF_LEN 256
char *stringToLowerCase(char *arr, int arr_size);
int valid_date(int date, int mon);

void show(int n, char *input_month)
{
    int k = 0, c = 0;
    int year = 2020;
    int diffmonth = 0;
    char sub[1000];
    char buf[BUF_LEN] = {0};
    char mon[BUF_LEN] = {0};
    char date[BUF_LEN] = {0};
    int totalnumberofdays = 0, numberofmonth = 0, findmonth = 0;
    time_t seconds = time(NULL);
    int current_month, current_date;
    struct tm *ctm = localtime(&seconds);
    strftime(buf, BUF_LEN, "%Y-%m-%d", ctm);
    //puts(buf);
    char *months[12] = {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"};
    while (c < 3)
    {
        sub[c] = input_month[c];
        c++;
    }
    sub[c] = '\0';
    //printf("Required substring is \"%s\"\n", sub);
    stringToLowerCase(sub, sizeof(sub));
    while (k < 12)
    {
        if (strcmp(months[k], sub) == 0)
        {
            // printf("%d-%d \n", k + 1, n);
            strftime(mon, BUF_LEN, "%m", ctm);
            strftime(date, BUF_LEN, "%d", ctm);
            current_month = atoi(mon);
            current_date = atoi(date);
            // printf("%d\n", current_month);
            // printf("%d\n", current_date);
            findmonth = k + 1;
            diffmonth = findmonth - current_month;
            if ((diffmonth == 0 && n < current_date) || diffmonth < 0)
            {
                diffmonth = 12 + diffmonth;
                year = 2021;
            }

            break;
        }
        k++;
    }
    if (!valid_date(n, findmonth))
    {
        printf("Date is invalid.\n");
        exit(0);
    }

    for (int i = 0; i < diffmonth; i++)
    {
        if (current_month == 2)
        {
            numberofmonth += 28;
        }
        else if (current_month == 4 || current_month == 6 || current_month == 9 || current_month == 11)
        {
            numberofmonth += 30;
        }
        else
        {
            numberofmonth += 31;
        }
        current_month += 1;
        current_month %= 12;
        // findmonth++;
    }

    totalnumberofdays = n + numberofmonth - current_date;
    // printf("\ncurrentDate: %d", current_date);
    // printf("\nDifference between months are %d and dates are %d\n", diffmonth, totalnumberofdays);
    printf("%d-%d-%d in %d days\n", year, findmonth, n, totalnumberofdays);
}

int main()
{
    int r, day = 0;
    int count = 0;
    char month[20] = {0};    
    while (1)
    {
        if (count == 2)
        {
            count = 0;
           
           show(day, month);
        }
        r = scanf("%d", &day);    
       

        if (r > 0)
        {
            ++count;
            // printf("Read Character, Continue\n");
            continue;
        }
        if (r == EOF){
            // printf("Format error, Breaking \n");
            break;
        }           
        
        r = scanf("%10s", month);
        // if (r > 0){
        //     printf("Date : %d\n", day);
        //     printf("month : %s\n", month);  
        //     show(day, month);  
        // }       

        ++count;
    }
    
    return 0;
}

char *stringToLowerCase(char *arr, int arr_size)
{
    int c = 0;
    char temp;
    while (c < arr_size)
    {
        temp = tolower(arr[c]);
        arr[c] = temp;
        c++;
    }
}

int valid_date(int day, int mon)
{
    int is_valid = 1;
    if (mon >= 1 && mon <= 12)
    {
        if (mon == 2)
        {
            if (day > 28)
            {
                is_valid = 0;
            }
        }
        else if (mon == 4 || mon == 6 || mon == 9 || mon == 11)
        {
            if (day > 30)
            {
                is_valid = 0;
            }
        }
        else if (day > 31)
        {
            is_valid = 0;
        }
    }

    return is_valid;
}