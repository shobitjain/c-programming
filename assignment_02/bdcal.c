#include <stdio.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define BUF_LEN 256
char *stringToLowerCase(char *arr, int arr_size);
int valid_date(int date, int mon, int calculated_year);

void show(int n, char *input_month)
{
    int k = 0, c = 0;
    //int year = 2020;
    int diffmonth = 0;
    char sub[1000];
    char buf[BUF_LEN] = {0};
    char mon[BUF_LEN] = {0};
    char date[BUF_LEN] = {0};
    char year[BUF_LEN] = {0};
    int totalnumberofdays = 0, numberofmonth = 0, findmonth = 0;
    time_t seconds = time(NULL);
    int current_month, current_date, calculated_year;
    struct tm *ctm = localtime(&seconds);
    strftime(buf, BUF_LEN, "%Y-%m-%d", ctm);
    strftime(year, BUF_LEN, "%Y", ctm);
    calculated_year = atoi(year);
    //puts(buf);
    char *months[12] = {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"};
    while (c < 3)
    {
        sub[c] = input_month[c];
        c++;
    }
    sub[c] = '\0';
    //printf("Required substring is \"%s\"\n", sub);
    stringToLowerCase(sub, sizeof(sub));
    while (k < 12)
    {
        if (strcmp(months[k], sub) == 0)
        {
            // printf("%d-%d \n", k + 1, n);
            strftime(mon, BUF_LEN, "%m", ctm);
            strftime(date, BUF_LEN, "%d", ctm);
            current_month = atoi(mon);
            current_date = atoi(date);
            // printf("%d\n", current_month);
            // printf("%d\n", current_date);
            findmonth = k + 1;
            diffmonth = findmonth - current_month;
            if ((diffmonth == 0 && n < current_date) || diffmonth < 0)
            {
                diffmonth = 12 + diffmonth;
                calculated_year = calculated_year + 1;
            }

            break;
        }
        k++;
    }
    if (!valid_date(n, findmonth, calculated_year))
    {
        printf("Date is invalid.\n");
        exit(0);
    }

    for (int i = 0; i < diffmonth; i++)
    {
        if (current_month == 2)
        {
            if ((calculated_year % 4 == 0 && calculated_year % 100 != 0) || (calculated_year % 400 == 0))
            {
                numberofmonth += 29;
            }
            else
            {
                numberofmonth += 28;
            }
        }
        else if (current_month == 4 || current_month == 6 || current_month == 9 || current_month == 11)
        {
            numberofmonth += 30;
        }
        else
        {
            numberofmonth += 31;
        }
        current_month += 1;
        current_month %= 12;
        // findmonth++;
    }

    totalnumberofdays = n + numberofmonth - current_date;
    // printf("\ncurrentDate: %d", current_date);
    // printf("\nDifference between months are %d and dates are %d\n", diffmonth, totalnumberofdays);
    //char* monthp, datep;
    if (findmonth < 10 && n < 10)
    {
        //monthp[0] = '0';
        // itoa(findmonth, monthp, 2);
        printf("%d-0%d-0%d in %d days\n", calculated_year, findmonth, n, totalnumberofdays);
    }
    else if (findmonth < 10 && n > 10)
    {
        //monthp[0] = '0';
        // itoa(findmonth, monthp, 2);
        printf("%d-0%d-%d in %d days\n", calculated_year, findmonth, n, totalnumberofdays);
    }
    else if (findmonth > 10 && n < 10)
    {
        //monthp[0] = '0';
        // itoa(findmonth, monthp, 2);
        printf("%d-%d-0%d in %d days\n", calculated_year, findmonth, n, totalnumberofdays);
    }
    else
        printf("%d-%d-%d in %d days\n", calculated_year, findmonth, n, totalnumberofdays);
    //printf("%d-%s-%d in %d days\n", year, findmonth, n, totalnumberofdays);
}

int main()
{
    int r, n = 0;
    int count = 0;
    char txt[10] = {0};
    while (1)
    {
        if (count == 2)
        {
            count = 0;
            show(n, txt);
        }
        r = scanf("%d", &n);
        if (r > 0)
        {
            ++count;
            continue;
        }
        if (r == EOF)
            break;

        r = scanf("%10s", txt);
        ++count;
    }
    return 0;
}

char *stringToLowerCase(char *arr, int arr_size)
{
    int c = 0;
    char temp;
    while (c < arr_size)
    {
        temp = tolower(arr[c]);
        arr[c] = temp;
        c++;
    }
}

int valid_date(int day, int mon, int calculated_year)
{
    int is_valid = 1, is_leap = 0;
    if ((calculated_year % 4 == 0 && calculated_year % 100 != 0) || (calculated_year % 400 == 0))
    {
        is_leap = 1;
    }

    if (mon >= 1 && mon <= 12)
    {
        if (mon == 2)
        {
            if (day > 28)
            {
                is_valid = 0;
            }
            else if (is_leap && day == 29)
            {
                is_valid = 0;
            }
        }
        else if (mon == 4 || mon == 6 || mon == 9 || mon == 11)
        {
            if (day > 30)
            {
                is_valid = 0;
            }
        }
        else if (day > 31)
        {
            is_valid = 0;
        }
    }

    return is_valid;
}